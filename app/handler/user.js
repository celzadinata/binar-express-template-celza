const { user } = require('../model/user')

const showUser = async (req, res) => {
  try {
    const dbUser = await user.findAll()
    if(dbUser.length < 1) {
      res.status(404).json({
        message: "user data not found",
        data: dbUser
      }).end()
    }
    
    res.status(200).json({
      message: "user data successfully fetched",
      data: dbUser
    }).end()
  }catch(err){
    console.log(err)
    res.status(503).json({
      message: "internal server error",
      data: null
    })
  }
}

module.exports = {
  showUser
}